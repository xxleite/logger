#!/bin/bash

#
#
#
#
#

export LC_NUMERIC="en_US.UTF-8"
export PATH=/usr/sbin:/usr/local/sbin:$PATH

R=6371000
PI=$(echo "(4*a(1/5) - a(1/239))*4" | bc -l)
PI_R=$(echo "$PI / 180" | bc -l)
CONTEXT=
CONTEXT_ID=
DEBUG=

#
# err
#

err () {
  echo "$(tput bold)$(tput setaf 1)$(tput setab 0)$1$(tput sgr0)" >&2
  [ -z $2 ] || exit 1
}

join () {
  local IFS="$1"; shift; echo "$*"
}

ord () {
  test ${#1} -eq 1 && printf %d "'$1" || echo $1 | sed -e "s#\(.\)#printf \"%d \" \"'\1\";#ge"
}

chr () {
  test ${#1} -eq 1 && echo -ne \\0$(printf %03o $1) || \
    echo $1 | sed -e "s#\([0-9]*\)#printf \\\\\\\\$\(printf %o \1\);#ge"
}

timestamp () {
  local t="$1"; test -z "$t" && read t
  local v=$(date -d "$t" +%s 2>&1)
  test -z "${v##*invalid*}" && echo 0 || echo $v
  #[[ "$v" == *"invalid"* ]] && echo 0 || echo $v
}

# see: https://www.movable-type.co.uk/scripts/latlong.html
#
# haversine lat lon lat2 lon2

haversine () {
  ( bc -l | xargs printf %.0f\\n ) <<MATH
    dlat = ($3 - $1) * $PI_R
    dlon = ($4 - $2) * $PI_R
    a = s(dlat/2) * s(dlat/2) + s(dlon/2) * s(dlon/2) * c($1 * $PI_R) * c($3 * $PI_R)
    (2 * a( sqrt(a) / sqrt(1-a) ) * $R) + 0.5
MATH
}

# see: https://www.giangrandi.org/electronics/radio/qthloccalc.shtml
#
# coords_to_grid lat lon

coords_to_grid () {
  chr "$(bc <<MATH
    lat=$1 + 90
    lon=$2 + 180
    (lon / 20) + 97;         (lat / 10) + 97
    (lon % 20 / 2) + 48;     (lat % 10 / 1) + 48
    (lon % 2 * 12) / 1 + 97; (lat % 1 * 24) / 1 + 97
MATH
)"
}

# see: https://www.giangrandi.org/electronics/radio/qthloccalc.shtml
#
# grid_to_coords qth_grid

grid_to_coords () {
  local res=($(ord $1))
  echo "[[$1]]" >&2
  ( bc -l | xargs printf "%.3f %.3f" ) <<MATH
    lat=((${res[1]} - 97) * 10) + ((${res[3]} - 48) * 1)
    lon=((${res[0]} - 97) * 20) + ((${res[2]} - 48) * 2)
    lat=(lat + (${res[5]} - 97) * 2.5 / 60) + 0.5 * 2.5 / 60 - 90
    lon=(lon + (${res[4]} - 97) * 5 / 60) + 0.5 * 5 / 60 - 180
    lat; lon
MATH
}

query () {
  local sql=""
  test -z "$1" && {
    IFS=''; while read -r line; do
      sql+="$line$(test -n "$DEBUG" && echo "\n" || echo "")"
    done
  } || sql="$@"

  test -z "${sql##*INSERT INTO*}" && sql="$sql; SELECT last_insert_rowid()"

  test -n "$DEBUG" && echo -ne "$(tput setaf 2)$sql\n$(tput sgr0)" >&2

  sqlite3 foo.db "$(echo -ne "$sql" | tr '\n' ' ')"
}

empty () {
  read -r line; test -z "$line" || test "$line" -eq 0
}

not_empty () {
  ! empty
}

seed () {
  query "SELECT COUNT(*) FROM bands" | empty && query <<SQL
    INSERT INTO bands (starts, ends, band) VALUES (135, 137, '2200M'), (472, 479, '630M'),
      (1800, 1850, '160M'), (3500, 3800, '80M'), (5351, 5366, '60M'), (7000, 7300, '40M'),
      (10000, 10500, '30M'), (14000, 14350, '20M'), (18000, 18500, '17M'), (21000, 21500, '15M'),
      (24500, 25000, '12M'), (28000, 29700, '10M'), (50000, 54000, '6M'), (144000, 148000, '2M'),
      (220000, 225000, '1.3M'), (430000, 440000, '70CM');
SQL
  
  query "SELECT COUNT(*) FROM modes" | empty && query <<SQL
    INSERT INTO modes (mode) VALUES ('CW'), ('SSB'), ('AM'), ('FM'), ('DV');

    INSERT INTO mode_ranges (mode_id, starts, ends) VALUES (1, 135, 137), (1, 472, 479),
      (1, 1800, 2000), (1, 3500, 4000), (1, 5351, 5356), (1, 7000, 7300), (1, 10000, 10500),
      (1, 14000, 14350), (1, 18000, 18500), (1, 21000, 21500), (1, 24500, 25000), (1, 28000, 29510),
      (1, 29590, 29620), (1, 50000, 51110), (1, 51500, 51610), (1, 52000, 54000), (1, 144000, 144600),
      (1, 144900, 145200), (1, 145500, 145565);
SQL

  # dummy data
  query "SELECT COUNT(*) FROM contests" | empty && query <<SQL
    INSERT INTO contests (name, starts, ends) VALUES
      ('BSB VHF Contest', 10, 100);

    INSERT INTO contest_bands (contest_id, band_id) VALUES
      (1, 13), (1, 14);

    INSERT INTO contest_modes (contest_id, mode_id) VALUES
      (1, 1), (1, 2), (1, 4);
SQL

}

init_db () {
  query <<SQL
    CREATE TABLE IF NOT EXISTS bands (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      starts INTEGER,
      ends INTEGER,
      band TEXT
    );

    CREATE TABLE IF NOT EXISTS modes (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      mode TEXT
    );

    CREATE TABLE IF NOT EXISTS mode_ranges (
      starts INTEGER,
      ends INTEGER,
      mode_id INTEGER,
      FOREIGN KEY (mode_id) REFERENCES modes (id)
    );

    CREATE TABLE IF NOT EXISTS contests (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      starts INTEGER,
      ends INTEGER
    );

    CREATE TABLE IF NOT EXISTS contest_bands (
      band_id INTEGER,
      contest_id INTEGER,
      FOREIGN KEY (band_id) REFERENCES bands (id),
      FOREIGN KEY (contest_id) REFERENCES contests (id)
    );

    CREATE TABLE IF NOT EXISTS contest_modes (
      mode_id INTEGER,
      contest_id INTEGER,
      FOREIGN KEY (mode_id) REFERENCES modes (id),
      FOREIGN KEY (contest_id) REFERENCES contests (id)
    );

    CREATE TABLE IF NOT EXISTS grids (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      lat TEXT,
      lon TEXT,
      address TEXT
    );

    CREATE TABLE IF NOT EXISTS groups (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      callsign TEXT
    );

    CREATE TABLE IF NOT EXISTS operators (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      name TEXT,
      callsign TEXT,
      group_id INTEGER,
      grid_id INTEGER,
      myself BOOLEAN,
      FOREIGN KEY (group_id) REFERENCES groups (id),
      FOREIGN KEY (grid_id) REFERENCES grids (id)
    );

    CREATE TABLE IF NOT EXISTS contacts (
      frequency INTEGER,
      time INTEGER,
      operator_id INTEGER,
      contest_id INTEGER,
      power INTEGER,
      rst INTEGER,
      FOREIGN KEY (operator_id) REFERENCES operators (id),
      FOREIGN KEY (contest_id) REFERENCES contests (id)
    );
SQL

  seed
}

#
# save_op qra callsign group gcallsign grid
#

save_op () {
  echo -ne "\nqra:$1 call:$2 dx:$3 dxcall:$4 qth:$5\n" >&2

  query "SELECT COUNT(*) FROM operators WHERE myself = true" | empty && \
    query "INSERT INTO operators (name, callsign, myself) VALUES ('${1/-/}', '${2/-/}', true)"

  { IFS='|'; read op_id; read group_id; } < <(query "SELECT id, group_id FROM operators WHERE myself = true")

  test -z "$group_id" && {
    test -z "${3/-/}" || group_id=$(query "INSERT INTO groups (name) VALUES ('$3')")
    test -z "${4/-/}" || group_id=$(query "INSERT INTO groups (callsign) VALUES ('$4')")
  } || {
    test -z "${3/-/}" || query "UPDATE groups SET name = '$3' WHERE id = $group_id"
    test -z "${4/-/}" || query "UPDATE groups SET callsign = '$4' WHERE id = $group_id"
  }

  test -z "${5/-/}" || {
    grid_id=$(query "SELECT id FROM grids WHERE address = '$5'")
    IFS=' '; read -a coords < <(grid_to_coords "$5")
    test -z "$grid_id" && \
      grid_id=$(query "INSERT INTO grids (lat, lon, address) VALUES (${coords[0]}, ${coords[1]}, '$5')")
  }

  local params=()
  test -z "$group_id" || params+=("group_id = $group_id")
  test -z "$grid_id"  || params+=("grid_id = $grid_id")
  test -z "${1/-/}"   || params+=("name = '$1'")
  test -z "${2/-/}"   || params+=("callsign = '$2'")

  query "UPDATE operators SET $(join "," "${params[@]}") WHERE id = $op_id"
}


# join_save 
#
# join_save items id table join_table query_field

join_save () {
  local params=()

  while read -r -a row; do
    params+=("($2, ${row[0]})")
  done < <(query "SELECT id FROM $4 WHERE $5 IN ($1)")

  test "${#params[@]}" -eq 0 || {
    query "DELETE FROM ${3::-1}_$4 WHERE ${3::-1}_id = $2"
    query "INSERT INTO ${3::-1}_$4 (${3::-1}_id, ${4::-1}_id) VALUES $(join "," "${params[@]}")"
  }
}


save_ctx () {
  echo -ne "\nctx:$1 bands:$2 modes:$3 start:$4 end:$5\n"

  test -z "$EDITING_CONTEST" && test -n "$1" && {
    EDITING_CONTEST=$(query "SELECT id FROM contests WHERE name = '$1'")
    test -z "$EDITING_CONTEST" && \
      EDITING_CONTEST=$(query "INSERT INTO contests (name) VALUES ('$1')") 
  }

  test -z "$EDITING_CONTEST" && {
    echo " cannot edit contest, please consider setting contest name first"
  } || {
    local params=()
    echo " ...[1] " 2>&1
    # set start and end date/time
    test -n "${4/-/}" && timestamp "$4" | not_empty && params+=("starts = $(timestamp "$4")")
    test -n "${5/-/}" && timestamp "$5" | not_empty && params+=("ends = $(timestamp "$5")")
    echo " ...[2] " 2>&1
    # list all bands
    test -z "${2/-/}" || {
      local bands=$(echo "$2," | tr -d ' ' | sed "s/\([0-9\.]\+\)\,/\1M,/g; s/\([0-9\.]\+[cCmM]\+\),/'\1',/g")
      join_save "${bands::-1}" $EDITING_CONTEST "contests" "bands" "band"
    }
    echo " ...[3] " 2>&1
    # list all modes
    test -z "${3/-/}" || {
      local modes=$(echo "$3," | tr -d ' ' | sed "s/\(\w\+\),*/'\1',/g")
      join_save "${modes::-1}" $EDITING_CONTEST "contests" "modes" "mode"
    }
    echo " ...[4] " 2>&1
    test "${#params[@]}" -eq 0 || \
      query "UPDATE contests SET $(join "," "${params[@]}") WHERE id = $EDITING_CONTEST"
  }
}

contests () {
  echo " ... contests ... "
}

header () {
  local h=$(date +%H)
  local gretting=$(
    test $h -lt 12 && echo "Good morning" || {
      test $h -lt 18 && echo "Good afternoon" || echo "Good evening"
  })

  # get current contest and qsos
#  (query | { IFS='|'; while read -r -a row; do
#
#  done }) <<SQL
#  SELECT 
#SQL

  # get operator data and group
  (query | { IFS='|'; while read -r -a row; do
    test -z "${row[0]}" || test -z "${row[1]}" && { #[[ -z "${row[0]}" || -z "{$row[1]}" ]] && {
      #[[ -z "${row[0]}" && -n "$1" ]] && echo "Please inform your name to log"
      #[[ -z "${row[1]}" && -n "$1" ]] && echo "Please inform your callsign to log"
      test -z "${row[0]}" && test -n "$1" && echo "Please inform your name to log"
      test -z "${row[1]}" && test -n "$1" && echo "Please inform your callsign to log"
    } || {
      #msg=$([ -n "${row[4]}" ] && echo "@ (${row[4]})")
      #[[ -n "${row[2]}" || -n "${row[3]}" ]] && \
      #  msg="$msg dxgroup: ${row[2]}$([ -n "${row[3]}" ] && echo " [${row[3]}]")"
      msg=$(test -n "${row[4]}" && echo "@ (${row[4]}")
      test -n "${row[2]}" || test -n "${row[3]}" && \
        msg="$msg dxgroup: ${row[2]}$(test -n "${row[3]}" && echo " [${row[3]}]")"

      echo -ne "\n $gretting, ${row[0]} [${row[1]}] $msg"
    }
  done }) <<SQL
  SELECT o.name, o.callsign, g.name AS gname, g.callsign AS gcallsign, address FROM operators o
    LEFT JOIN groups g ON (o.group_id = g.id)
    LEFT JOIN grids qth ON (o.grid_id = qth.id)
    WHERE myself = true
SQL
}

help () {
  cat <<EOF

    Simple QSO logger

    press:
      ESC          - undo or quit
      ENTER        - assign or enter command
      TAB          - assign or move to next command part
      >            - move right to navigate options, go to next page
      <            - move left to navigate options, go to last page

    commands:
      general purpose:
        > help [ENTER]                        - this help
        > qso [ENTER]                         - list QSOs
        > contests [ENTER]                    - list contests
        > py2mzm [ENTER]                      - statistics about QSO with py2mzm
        > gg66sj [ENTER]                      - statistics about this grid
        > -23.6565 -46.7587 [ENTER]           - return grid based on latitude and longitude
        > d:32 [ENTER]                        - delete current context with id 32
        > e:42 [ENTER]                        - edit current context with id 42
        > p:2 [ENTER]                         - paginate current context to page 2

      operator's info:
        > call:py2aaa [ENTER]                 - assign operator's callsign
        > qra:Maria da Silva [ENTER]          - assign operator's QRA
        > qth:gg66ss [ENTER]                  - assign operator's QTH grid location
        > dx:Rancho Queimado [ENTER]          - assign operator's DX group
        > dxcall:PPRDA [ENTER]                - assign operator's DX group callsign

      contest's info:
        > ctx:BSB VHF Contest [ENTER]         - assign contest
        > bands:6M,2M [ENTER]                 - current contest bands
        > modes:CW,FM,SSB [ENTER]             - current contest modes
        > start:2021-06-25T00:00:00Z [ENTER]  - current contest starting date and time, UTC format
        > end:2021-06-30T18:00:00Z [ENTER]    - current contest ending date and time, UTC format

      logging:
        > FM [ENTER]                          - assign current operation mode, valids are: FM, AM, SSB, CW
        > 146610 [ENTER]                      - assign current frequency (in KHz)
        > py2mzm 59 59 [ENTER]                - log QSO with py2mzm
        > py2qb 59 59 gg66sj [ENTER]          - log QSO with py2qb together with grid location
        > py2mzm:Ronaldo [ENTER]              - log py2mzm QRA
        > c:São Paulo, SP /MOVEL [ENTER]      - log comments about last QSO


EOF
}

read_loop () {
  local IFS=''

  local txt=""
  tput sc

  local v=()
  local t=$(stty -g)
  stty -echo
  printf "\033[6n"
  IFS='[;' read -ra v -d R
  stty $t
  local pos=(${v[@]:1})

  while :; do
    IFS= read -s -N 1 -t 1 -r key

    case $key in
      $'\e')   echo -e "\n [ESC]"; break ;;
      $'\x0a')
        local list=()

        test -z "${txt##*:*}" && {
          local index=-1
          while read item; do
            test -z "${item##*:*}" && {
              index=$((index+1))
              list[$index]="$(echo "${item%:*}" | tr '[:upper:]' '[:lower:]'):${item#*:}"
            } ||  list[$index]="${list[$index]} $item"
          done < <(echo $txt | tr ' ' '\n')
        } || list[0]=$txt

        # > ctx:BSB VHF Contest [ENTER]         - assign contest
        # > bands:6M,2M [ENTER]                 - current contest bands
        # > modes:CW,FM,SSB [ENTER]             - current contest modes
        # > start:2021-06-25T00:00:00Z [ENTER]  - current contest starting date and time, UTC format
        # > end:2021-06-30T18:00:00Z [ENTER]    - current contest ending date and time, UTC format

        for item in "${list[@]}"; do
          item=$(echo $item | xargs)
          case $item in
            "help")                   header;  help ;;
            "contests")               contests ;;
            fm | am | ssb | cw | dv)  mode=$item ;;
            "qra:"*)                  save_op "${item/qra:/}" ;;
            "call:"*)                 save_op - "${item/call:/}" ;;
            "dx:"*)                   save_op - - "${item/dx:/}" ;;
            "dxcall:"*)               save_op - - - "${item/dxcall:/}" ;;
            "qth:"*)                  save_op - - - - "${item/qth:/}" ;;
            "ctx:"*)                  save_ctx "${item/ctx:/}" ;;
            "bands:"*)                save_ctx - "${item/bands:/}" ;;
            "modes:"*)                save_ctx - - "${item/modes:/}" ;;
            "start:"*)                save_ctx - - - "${item/start:/}" ;;
            "end:"*)                  save_ctx - - - - "${item/end:/}";;
            *)
              echo -e "\n [Enter]"; break ;;
          esac
        done
        txt=''
        ;;
      $'\t')   echo -e "\n [Tab]"; break ;;
      $'\177')
        key=""
        txt="${txt::-1}"
        ;;
      C)    echo -e "\n [>]"; break ;;
      D)    echo -e "\n [<]"; break ;;
      *)
        txt+=$key
    esac

#    echo $((2+${#txt})) >&2
#    printf "\r> $txt                          "
    tput rc; tput ed
    printf "> $txt" # BOO"
    tput cup $((${pos[0]}-1)) $((2+${#txt}))
  done

  echo $txt
}

# reset getopts
#OPTIND=1
# options
while getopts "h?v" opt; do
  case "$opt" in
    h|\?) help; exit 0 ;;
    v)    DEBUG=1 ;;
  esac
done

init_db

read_loop
